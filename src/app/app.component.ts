import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { AuthService } from './services';
import { User } from './models/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'mvideo-test';
  time: number | null = null;
  user: User | null = null;

  constructor(
    private activatedRoute: ActivatedRoute,
    private authService: AuthService
  ) {
    this.authService.user.subscribe(x => this.user = x);
    this.activatedRoute.queryParams.subscribe(data => {
      if (data.timer) {
        this.time = Number.parseInt(data.timer);
      }
    })
  }
}
