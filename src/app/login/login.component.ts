﻿import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Subscription, interval } from 'rxjs';

import { AuthService } from '@app/services';
import { User } from '@app/models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  
  private subscription: Subscription;
  
  loginForm!: FormGroup;
  loading: boolean = false;
  submitted: boolean = false;
  error: string = '';
  timer?: number;
  public secondsToUnlock: number = 0;
  public timeDifference?: number;

  @Input() blockTime: number | null = null;
  @Input() user: User | null = null;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) {}

  ngOnChanges() {
    if(this.blockTime) {
      this.subscription = interval(1).subscribe(x => { this.getTimeDifference();});
    }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
        username: ['', Validators.required],
        password: ['', Validators.required]
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private getTimeDifference () {
    if(this.blockTime) {
      this.loading = true;
      this.timeDifference = (this.blockTime + 60000) - new Date().getTime();
      this.allocateTimeUnits(this.timeDifference);
      if(this.secondsToUnlock < 0) {
        this.blockTime = null;
        this.loading = false;
        this.router.navigate(['/']);
        this.subscription.unsubscribe();
      }
    }
  }

  private allocateTimeUnits (timeDifference: number) {
    this.secondsToUnlock = Math.floor((timeDifference) / 1000 % 60);
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
        return;
      }
      this.loading = true;
      this.authService.login(this.f.username.value, this.f.password.value)
        .pipe(first())
        .subscribe({
          next: () => {
            this.loading = false;
          },
          error: error => {
            this.timer = new Date().getTime();
            this.router.navigate(['/'], {queryParams: {timer: this.timer}});
            this.error = error;
            setTimeout(() => { this.error = '' }, 5000)
          }
      });
    }

  logout() {
    this.authService.logout();
  }
}
